package com.kru13.httpserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.Semaphore;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class SocketServer implements Runnable {

	static final String DEFAULT_FILE = "index.html";
	static final String METHOD_NOT_SUPPORTED = "not_supported.html";
	static Semaphore semaphore;
	static public int count=0;
	static public int data=0;

	static public Activity activity;

	static ServerSocket serverSocket;
	public static final int port = 12345;
	static boolean bRunning = true;
	//View v;

	private Socket s;

	public SocketServer(Socket s1) {
		s = s1;
	}

	public void close() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			Log.d("SERVER", "Error, probably interrupted in accept(), see log");
			e.printStackTrace();
		}
	}

	public int getCount()
	{
		return this.count;
	}

	public SocketServer(Activity act) {
		//this.v = v;
		this.activity = act;
		semaphore = new Semaphore(1);
		try {
			ServerSocket serverConnect = new ServerSocket(port);
			Log.d("SERVER", "Connected: " + count + ", amount of data transfered (kB): " + data);

			// we listen until user halts server execution
			while (true) {
				Log.d("SERVER", "Socket Waiting for connection");
				SocketServer myServer = new SocketServer(serverConnect.accept());

				if (bRunning) {
					Log.d("SERVER", "Socket Accepted");
					this.count = this.count + 1;
					Log.d("SERVER", "Connected: " + count + ", amount of data transfered (kB): " + data);
				}

				// create dedicated thread to manage the client connection
				Thread thread = new Thread(myServer);

				semaphore.acquire();
				thread.start();
			}

		} catch (IOException e) {
			if (serverSocket != null && serverSocket.isClosed())
				Log.d("SERVER", "Normal exit");
			else {
				Log.d("SERVER", "Error");
				e.printStackTrace();
			}
		}
	 	catch (InterruptedException e) {
		e.printStackTrace();
		}
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	public void run() {
		OutputStream o = null;
		BufferedWriter out = null;
		BufferedReader in = null;
		String fileRequested = null;

		try {
			o = s.getOutputStream();
			out = new BufferedWriter(new OutputStreamWriter(o));
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));

			String input = in.readLine();
			StringTokenizer parse = new StringTokenizer(input);
			String method = parse.nextToken().toUpperCase();


			if (!method.equals("GET") && !method.equals("POST")) {
				notImplemented(o, out, method);
			} else {
				fileRequested = parse.nextToken();
				// GET or POST method
				if (fileRequested.endsWith("/")) {
					fileRequested += DEFAULT_FILE;
				}

				String URI = fileRequested;
				String SDpath = Environment.getExternalStorageDirectory().getAbsolutePath().toString();
				String fileName = SDpath + URI;

				File file = new File(fileName);
				File testIndex = new File(SDpath + "/" + DEFAULT_FILE);
				boolean dir = false;

				if (file.isDirectory()) {
					dir = true;
				}

				if (dir == false && !file.exists() && !fileRequested.equals("/" + DEFAULT_FILE)) {
					//Log.d("SERVER", "dir is: " + dir);
					fileNotFound(o, out, fileRequested);
				} else if (dir == false && !file.exists() && fileRequested.equals("/" + DEFAULT_FILE)) {
					File SD = new File(SDpath);
					listContent(o, out, SD);
				} else {
					if (method.equals("GET")) {
						get(o, out, file, fileName);
					}
					if (method.equals("POST")) {
						//Log.d("SERVER", "Received POST: ");
						post(in, out);
					}
				}
			}

		} catch (FileNotFoundException fnfe) {
			try {
				fileNotFound(o, out, fileRequested);
			} catch (IOException ioe) {
				Log.d("SERVER", "Error");
				fnfe.printStackTrace();
			}

		} catch (NullPointerException e) {
			try {
				fileNotFound(o, out, fileRequested);
			} catch (IOException ioe) {
				Log.d("SERVER", "Error");
				e.printStackTrace();
			}
		} catch (IOException ioe) {
			System.err.println("Server error : " + ioe);
		} finally {
			try {
				in.close();
				out.close();
				o.close();
				semaphore.release();
				count = count - 1;
				Log.d("SERVER", "Connected: " + count + ", amount of data transfered (kB): " + data);
				s.close(); // we close socket connection
			} catch (Exception e) {
				Log.d("SERVER", "Error");
				e.printStackTrace();
			}

			if (bRunning) {
				Log.d("SERVER", "Connection closed");

			}
		}
	}

	private void fileNotFound(OutputStream o, BufferedWriter out, String fileRequested) throws IOException {

		out.write("HTTP/1.0 404 Not found\n");
		out.write("Content-type: text/html\n");
		out.write("\n"); // blank line between headers and content, very important !
		out.write("<HTML><BODY><h2>Error 404</h2><p></p><p></p><p></p><p></p><p></p><h6>Autor: Josef Skulina</h6></BODY></HTML>");
		out.flush(); // flush character output stream buffer

		if (bRunning) {
			Log.d("SERVER", "File " + fileRequested + " not found");
		}
	}

	private void notImplemented(OutputStream o, BufferedWriter out, String method) throws IOException {
		if (bRunning) {
			Log.d("SERVER", "501 - " + method + " is not implemented");
		}

		String URI = METHOD_NOT_SUPPORTED;
		String SDpath = Environment.getExternalStorageDirectory().getAbsolutePath().toString();
		String fileName = SDpath + "/" + URI;

		// we return the not supported file to the client
		File file = new File(fileName);
		int fileLength = (int) file.length();
		String contentMimeType = "text/html";

		// we send HTTP Headers with data to client
		out.write("HTTP/1.1 501 Not Implemented");
		out.write("Server: OSMZ Server : 1.0");
		out.write("Date: " + new Date());
		out.write("Content-type: " + contentMimeType);
		out.write("Content-length: " + fileLength);
		out.write("\n"); // blank line between headers and content, very important !
		out.flush(); // flush character output stream buffer

		Log.d("SERVER", "FILENAME 501 - " + fileName);

		byte[] buffer = new byte[1024];
		int len;
		FileInputStream fis = new FileInputStream(file);

		while ((len = fis.read(buffer, 0, buffer.length)) > 0) {
			o.write(buffer, 0, len);
		}
		o.flush();

		data = (data + fileLength/1024);

	}

	private void get(OutputStream o, BufferedWriter out, File file, String fileName) throws IOException {
		String SDpath = Environment.getExternalStorageDirectory().getAbsolutePath().toString();
		String name = file.getPath().replace(SDpath, "");
		String nameFile = name.replace("/", "");

		if (file.isDirectory()) {
			if(nameFile.equals("screencap"))
			{
				Log.d("SERVER", "Filename is: " + nameFile);
				screenShot();
			}else{
			//Log.d("SERVER", "Filename is: " + fileName);
			listContent(o, out, file);
			}

		} else {

			int fileLength = (int) file.length();

			// GET method so we return content
			// send HTTP Headers
			out.write("HTTP/1.0 200 OK\n");
			String contentType = "text/html";

			if (fileName.endsWith("png")) contentType = "image/png";
			if (fileName.endsWith("jpg")) contentType = "image/jpeg";

			out.write("Content-type: " + contentType + "\n");
			out.write("Content-Length: " + String.valueOf(file.length()) + "\n");
			out.write("\n");
			out.flush();

			Log.d("SERVER", "FILENAME 200 - " + fileName);

			byte[] buffer = new byte[1024];
			int len;
			FileInputStream fis = new FileInputStream(file);

			while ((len = fis.read(buffer, 0, buffer.length)) > 0) {
				o.write(buffer, 0, len);
			}
			o.flush();

			if (bRunning) {
				Log.d("SERVER", "200 - " + fileName + " returned");
			}
			data = (data + (int)file.length()/1024);
		}
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	private void post(BufferedReader in, BufferedWriter out) throws IOException {
		String line;
		line = in.readLine();
		StringBuilder raw = new StringBuilder();
		raw.append("" + line);
		int contentLength = 0;

		while (!(line = in.readLine()).equals("")) {
			raw.append('\n' + line);

			final String contentHeader = "Content-Length: ";
			if (line.startsWith(contentHeader)) {
				contentLength = Integer.parseInt(line.substring(contentHeader.length()));
			}
		}

		data = (data + contentLength/1024);

		StringBuilder body = new StringBuilder();
		int c = 0;
		for (int i = 0; i < contentLength; i++) {
			c = in.read();
			body.append((char) c);
			//Log.d("SERVER", "POST: " + ((char) c) + " " + c);
		}
		raw.append(body.toString());

		String[] lines = body.toString().split("&");
		String[] lines2 = new String[8];
		String userName = "";
		String note = "";
		int i = 0;

		for (String s : lines) {
			lines2 = s.split("=");
			for (String s2 : lines2) {
				if (i == 1) {
					userName = s2;
				}
				if (i == 3) {
					note = s2;
				}
				//Log.d("SERVER", "POST - " + s2);
				i++;
			}
		}
		//Log.d("SERVER", "POST - jmeno: " + userName);
		//Log.d("SERVER", "POST - inzerat: " + note);

		String SDpath = Environment.getExternalStorageDirectory().getAbsolutePath().toString();
		String linee = null;
		List<String> linees = new ArrayList<String>();
		File f = new File(SDpath + "/inzerce.txt");

		//Log.d("SERVER", SDpath + "/inzerce.txt");

		if (!f.isFile()) {
			f.createNewFile();
			f.setReadable(true);
			f.setWritable(true);

			FileWriter fw = new FileWriter(f);
			BufferedWriter ouut = new BufferedWriter(fw);

			ouut.write(userName + " wrote: " + note.replace('+', ' '));
			ouut.flush();
			ouut.close();

		} else {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			while ((linee = br.readLine()) != null) {
				linees.add(linee);
				linees.add("\r\n");
			}
			fr.close();
			br.close();

			FileWriter fw = new FileWriter(f);
			BufferedWriter ouut = new BufferedWriter(fw);
			for (String s : linees) {
				ouut.write(s);
			}
			ouut.write(userName + " wrote: " + note.replace('+', ' '));
			ouut.flush();
			ouut.close();
		}


		out.write("HTTP/1.1 200 OK\r\n");
		out.write("Content-Type: text/html\r\n");
		out.write("\r\n");
		out.write("<html>\n\t<body>\n\t\t<h1>Inzerce pridana</h1>\n\t\t<ul>\n\t\t\t");
		out.write("<li> <a href=\"/\">" + "Zpet na hlavni stranku" + "</a> </li>\n");
		out.flush();
	}

	private void listContent(OutputStream o, BufferedWriter out, File file) throws IOException {
		String fileName = file.getPath() + "/output.html";
		String SDpath = Environment.getExternalStorageDirectory().getAbsolutePath().toString(); // /storage/sdcard/
		ArrayList<String> contents = new ArrayList<String>(Arrays.asList(file.list()));
		File output = new File(fileName);
		BufferedWriter writer = new BufferedWriter(new FileWriter(output));

		String path = file.getPath().replace(SDpath, "");
		String root = "/";
		String oneHigher = root;

		if (path.toString().length() != 0) {
			int count = 0;
			for (int i = 0; i < path.length(); i++) {
				if (path.charAt(i) == '/') {
					count++;
				}
			}
			if (count > 1) {
				int index = path.lastIndexOf("/");
				oneHigher = path.substring(0, index);
			}
		}

		try {
			writer.write("<html>\n\t<body>\n\t\t<h1>Contents of " + file.getCanonicalPath() + ":</h1>\n\t\t<ul>\n\t\t\t");
			writer.write("<li> <a href=\"" + root + "\">" + "." + "</a> </li>\n");
			writer.write("<li> <a href=\"" + oneHigher + "\">" + ".." + "</a> </li>\n");
			for (int counter = 0; counter < contents.size(); counter++) {
				writer.write("\t\t\t<li> <a href=\"" + path + "/" + contents.get(counter) + "\">" + contents.get(counter) + "</a> </li>\n");
			}
			writer.write("\t\t</ul>\n\t</body>\n</html>");
			writer.flush();
		} catch (IOException e) {
			Log.d("SERVER", "Error");
		}

		output = new File(fileName);

		out.write("HTTP/1.0 200 OK\n");
		String contentType = "text/html";

		if (fileName.endsWith("png")) contentType = "image/png";
		if (fileName.endsWith("jpg")) contentType = "image/jpeg";

		out.write("Content-type: " + contentType + "\n");
		out.write("Content-Length: " + String.valueOf(output.length()) + "\n");
		out.write("\n");
		out.flush();

		Log.d("SERVER", "FILENAME 200 - " + fileName);

		out.write("<html>\n\t<body>\n\t\t<h1>Contents of " + file.getCanonicalPath() + ":</h1>\n\t\t<ul>\n\t\t\t");
		out.write("<li> <a href=\"" + root + "\">" + "." + "</a> </li>\n");
		out.write("<li> <a href=\"" + oneHigher + "\">" + ".." + "</a> </li>\n");
		for (int counter = 0; counter < contents.size(); counter++) {
			out.write("\t\t\t<li> <a href=\"" + path + "/" + contents.get(counter) + "\">" + contents.get(counter) + "</a> </li>\n");
		}
		out.write("\t\t</ul>\n\t</body>\n</html>");
		out.flush();

		output.delete();

		data = (data + ((int)output.length())/1024);

	}

	private void screenShot()
	{
		//v.performClick();
		OutputStream o = null;
		BufferedWriter out = null;
		String SDpath = Environment.getExternalStorageDirectory().getAbsolutePath().toString();
		String fileName = SDpath +"/screen.jpg";
        File file = new File(fileName);
		Log.d("SERVER", "Filename is2: " + file.getName());
		try {
			o = s.getOutputStream();

		out = new BufferedWriter(new OutputStreamWriter(o));
		out.write("HTTP/1.0 200 OK\n");
		String contentType = "text/html";

		if (fileName.endsWith("png")) contentType = "image/png";
		if (fileName.endsWith("jpg")) contentType = "image/jpeg";

		out.write("Content-type: " + contentType + "\n");
		out.write("Content-Length: " + String.valueOf(file.length()) + "\n");
		out.write("\n");
		out.flush();

		Log.d("SERVER", "FILENAME 200 - " + fileName);

		byte[] buffer = new byte[1024];
		int len;
		FileInputStream fis = new FileInputStream(file);

		while ((len = fis.read(buffer, 0, buffer.length)) > 0) {
			o.write(buffer, 0, len);
		}
		o.flush();

		data = (data + ((int)file.length())/1024);

	}catch (IOException ioe) {
			System.err.println("Server error : " + ioe);
		}
	}
}
